# frozen_string_literal: true

module Mutations
  class FollowGenre < BaseMutation
    argument :token, String, required: true
    argument :genre_id, Integer, required: true

    field :follow, Types::FollowType, null: false
    field :errors, [String], null: false

    def resolve(token:, genre_id:)
      authorize_user
      user_id = User.find_by_authentication_token(token).id
      Genre.follow(user_id, genre_id)
      MutationResult.call
    end
  end
end
