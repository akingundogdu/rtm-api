# frozen_string_literal: true

module Mutations
  class SignIn < Mutations::BaseMutation
    graphql_name 'SignIn'

    argument :email, String, required: true
    argument :password, String, required: true

    field :user, Types::UserType, null: false

    def resolve(args)
      if args[:password].to_s == ''
        GraphQL::ExecutionError.new('Incorrect email or password')
      else
        User.sign_in(args, context)
      end
    end
  end
end
