# frozen_string_literal: true

module Mutations
  class ChangePassword < Mutations::BaseMutation

    argument :email, String, required: true
    argument :password, String, required: true

    def resolve(args)
      if args[:password].to_s == '' || args[:email].to_s == ''
        GraphQL::ExecutionError.new('Incorrect email or password')
      else
        User.change_password(args[:email], args[:password])
      end
    end
  end
end
