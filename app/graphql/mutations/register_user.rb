module Mutations
  class RegisterUser < BaseMutation
    argument :first_name, String, required: true
    argument :last_name, String, required: true
    argument :email, String, required: true
    argument :password, String, required: true
    argument :password_confirmation, String, required: true

    field :user, Types::UserType, null: false

    def resolve(args)
      if args[:password] != args[:password_confirmation]
        GraphQL::ExecutionError.new('The password and confirmation password should be same')
      else
        User.register_user(args, context)
      end
    rescue ActiveRecord::RecordInvalid => invalid
      message = invalid.record.errors.full_messages.join(', ')
      GraphQL::ExecutionError.new(message)
    end
  end
end
