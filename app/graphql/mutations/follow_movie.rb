# frozen_string_literal: true

module Mutations
  class FollowMovie < BaseMutation
    argument :token, String, required: true
    argument :movie_id, Integer, required: true

    field :follow, Types::FollowType, null: false
    field :errors, [String], null: false

    def resolve(token:, movie_id:)
      authorize_user
      user_id = User.find_by_authentication_token(token).id
      Movie.follow(user_id, movie_id)
      MutationResult.call
    end
  end
end
