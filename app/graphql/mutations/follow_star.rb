# frozen_string_literal: true

module Mutations
  class FollowStar < BaseMutation
    argument :token, String, required: true
    argument :star_id, Integer, required: true

    field :follow, Types::FollowType, null: false
    field :errors, [String], null: false

    def resolve(token:, star_id:)
      authorize_user
      user_id = User.find_by_authentication_token(token).id
      Star.follow(user_id, star_id)
      MutationResult.call
    end
  end
end
