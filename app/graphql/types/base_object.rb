module Types
  class BaseObject < GraphQL::Schema::Object
    field_class Types::BaseField
    protected
    def authorize_user
      RtmApiSchema.authorized(object, context)
    end
  end
end
