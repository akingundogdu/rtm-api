# frozen_string_literal: true

module Types
  class QueryType < Types::BaseObject
    field :users, [UserType], null: true
    def users
      authorize_user
      User.all
    end

    field :user, UserType, null: true do
      argument :token, String, required: true
    end
    def user(token:)
      authorize_user
      User.find_by_authentication_token(token)
    end

    field :check, String, null: true do
    end
    def check
      token = context[:current_user]
      user = User.find_by_authentication_token(token)
      if user.nil?
        ''
      else
        user.authentication_token
      end
    end

    field :stars, [StarType], null: true
    def stars
      authorize_user
      Star.all.limit(100)
    end
    field :star, StarType, null: true do
      argument :id, ID, required: true
    end
    def star(id:)
      authorize_user
      Star.find(id)
    end

    field :genres, [GenreType], null: true
    def genres
      authorize_user
      Genre.all
    end

    field :genre, GenreType, null: true do
      argument :id, ID, required: true
    end
    def genre(id:)
      authorize_user
      Genre.find(id)
    end

    field :movies, [MovieType], null: true do
      argument :by, String, required: true
    end
    def movies(by:)
      authorize_user
      Movie.get_movies by
    end
    field :movie, MovieType, null: true do
      argument :id, ID, required: true
    end
    def movie(id:)
      authorize_user
      Movie.find(id)
    end

    field :follows, FollowType, null: true do
      argument :followable_type, String, required: true
    end
    def follows(followable_type:)
      authorize_user
      Follow.find_by_followable_type(followable_type)
    end
    field :follow, FollowType, null: true do
      argument :id, ID, required: true
    end
    def follow(id:)
      authorize_user
      Follow.find(id)
    end

    field :suggestions, [SuggestionType], null: true do
      argument :token, String, required: true
    end
    def suggestions(token:)
      authorize_user
      Suggestion.get_suggestions token
    end
  end
end
