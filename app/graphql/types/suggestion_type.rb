module Types
  class SuggestionType < BaseObject
    field :user, UserType, null: false
    def user
      object.user
    end

    field :follow, FollowType, null: false
    def follow
      object.follow
    end

    field :movie, MovieType, null: false
    def movie
      object.movie
    end
  end
end