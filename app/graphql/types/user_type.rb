module Types
  class UserType < BaseObject

    implements GraphQL::Types::Relay::Node
    global_id_field :id

    field :id, ID, null: false
    field :first_name, String, null: false
    field :last_name, String, null: false
    field :email, String, null: false

    field :movies, [MovieType], null: true do
      argument :size, Int, required: false, default_value: 1000
    end
    def movies(size:)
      object.movies.order("RANDOM()").limit(size)
    end

    field :genres, [GenreType], null: true do
      argument :size, Int, required: false, default_value: 1000
    end
    def genres(size:)
      object.genres.limit(size)
    end

    field :stars, [StarType], null: true do
      argument :size, Int, required: false, default_value: 1000
    end
    def stars(size:)
      object.stars.limit(size)
    end

    field :authentication_token, String, null: false
    def authentication_token
      if RtmApiSchema.id_from_object(object, UserType) != RtmApiSchema.id_from_object(context[:current_user], UserType)
        raise GraphQL::UnauthorizedFieldError,
              "Unable to access authentication_token"
      end
      object.authentication_token
    end
  end
end