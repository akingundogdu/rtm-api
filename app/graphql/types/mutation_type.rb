module Types
  class MutationType < Types::BaseObject
    field :follow_movie, mutation: Mutations::FollowMovie
    field :follow_genre, mutation: Mutations::FollowGenre
    field :follow_star, mutation: Mutations::FollowStar
    field :register_user, mutation: Mutations::RegisterUser
    field :sign_in, mutation: Mutations::SignIn
    field :change_password, mutation: Mutations::ChangePassword
   end
end
