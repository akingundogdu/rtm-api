module Types
  class GenreType < BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :movies, [MovieType], null: true do
      argument :size, ID, required: false, default_value: 5
    end
    def movies(size:)
      object.movies.limit(size)
    end
  end
end