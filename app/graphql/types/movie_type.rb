module Types
  class MovieType < BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :summary, String, null: false
    field :year, Int, null: false
    field :rate, Float, null: false
    field :time, String, null: false
    field :director, String, null: false
    field :image, String, null: false

    field :stars, [StarType], null: false do
      argument :size, Int, required: false, default_value: 5
    end
    def stars(size:)
      object.stars.limit(size)
    end

    field :genres, [GenreType], null: false do
      argument :size, Int, required: false, default_value: 5
    end

    def genres(size:)
      object.genres.limit(size)
    end
  end
end