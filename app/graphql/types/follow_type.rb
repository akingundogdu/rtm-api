# frozen_string_literal: true

module Types
  class FollowType < BaseObject
    field :id, ID, null: false
    field :user, UserType, null: false
    field :genre, GenreType, null: true
    field :star, StarType, null: true
    field :movie, MovieType, null: true
    field :ako, GenreType, null: true do
      argument :size, ID, required: false, default_value: 5
    end

    def genre
      Genre.find(object.followable_id) if object.followable_type == Genre.name
    end

    def star
      Star.find(object.followable_id) if object.followable_type == Star.name
    end

    def movie
      Movie.find(object.followable_id) if object.followable_type == Movie.name
    end
  end
end
