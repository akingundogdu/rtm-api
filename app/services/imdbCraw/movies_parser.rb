# frozen_string_literal: true

require 'nokogiri'
require 'open-uri'

module ImdbCraw
  class MoviesParser
    def parse_all_movies_by(start_page_count)
      movies = get_movie_containers start_page_count
      movies.each do |movie|
        # create all models from parsed movie container elements
        created_movie = create_movie movie
        created_genres = create_genres movie
        created_stars = create_stars movie

        next if created_movie.nil?

        # assign created each models
        created_movie.genres << created_genres
        created_movie.stars << created_stars
      end
    end

    def create_movie(movie)
      name = get_movie_title movie
      summary = get_movie_summary movie
      image = get_movie_image movie
      year = get_movie_year movie
      rate = get_movie_rate movie
      time = get_movie_time movie
      director = get_movie_director movie
      created_movie = nil
      if image != false
        created_movie = Movie.create!(name: name,
                                      summary: summary,
                                      image: image,
                                      year: year,
                                      rate: rate,
                                      time: time,
                                      director: director)
      end
      created_movie
    end

    def create_genres(movie)
      genres = get_movie_genres movie
      data = []
      genres.each do |genre|
        value = genre.strip
        result = Genre.find_by_name value
        if result.nil?
          created = Genre.create!(name: value)
          data.push created
        else
          data.push result
        end
      end
      data
    end

    def create_stars(movie)
      stars = get_movie_stars movie
      data = []
      stars.each do |star|
        value = star.strip
        result = Star.find_by_name value
        if result.nil?
          created = Star.create!(name: value)
          data.push created
        else
          data.push result
        end
      end
      data
    end

    def get_movies_url_by(start_page_count = 0)
      @types = 'feature,tv_movie,tv_series,tv_episode'
      @groups = 'groups=top_1000'
      "https://www.imdb.com/search/title/?title_type=#{@types}&#{@groups}&count=250&start=#{(start_page_count + 1)}"
    end

    def get_page_title
      response = get_response
      response.css('.article .header').first.content
    end

    def check_first_page_item_count
      check_page_count 0, 250
    end

    def check_second_page_item_count
      check_page_count 250, 500
    end

    def check_third_page_item_count
      check_page_count 500, 750
    end

    def check_fourth_page_item_count
      check_page_count 750, 1000
    end

    def get_movie_containers(start_page_count = 0)
      response = get_response start_page_count
      movies_containers = response.css('.lister-list .lister-item')
      movies_containers
    end

    def get_first_movie_container
      movie_container = get_movie_containers
      movie_container.first
    end

    def get_response(start_page_count = 0)
      Nokogiri::HTML(open(get_movies_url_by(start_page_count)))
    end

    def get_movie_container_index(movie_container)
      row_index = movie_container.css('.lister-item-content .lister-item-header .lister-item-index').first.content
      row_index.gsub(',', '')
    end

    def get_movie_image(movie_container)
      image = movie_container.css('.lister-item-image .loadlate').first
      normal_image = image['loadlate']
      big_image = normal_image.split('@')[0] + '@' # for high quality images
      high_quality_image_path = big_image + '._V1_UY800_CR3,0,500,800_AL_.jpg'
      small_image_path = big_image + '._V1_UY10_CR0,0,1,1_AL_.jpg'
      open(small_image_path).read # this is checking the image can loadable, if not the movie will not create
      high_quality_image_path
    rescue OpenURI::HTTPError => e
      return false if e.to_s == '404 Not Found'
    end

    def get_movie_title(movie_container)
      title = movie_container.css('.lister-item-header a').first
      title.content
    end

    def get_movie_year(movie_container)
      year = movie_container.css('.lister-item-header .lister-item-year').first
      year.content.gsub('(', '').gsub(')', '').to_i
    end

    def get_movie_time(movie_container)
      time = movie_container.css('.lister-item-content .runtime').first
      time.content
    end

    def get_movie_genres(movie_container)
      genres = movie_container.css('.lister-item-content .genre').first
      genres.content.strip.split(',')
    end

    def get_movie_stars(movie_container)
      stars_container = movie_container.css('.lister-item-content p')[2]
      stars = []
      stars_container.css('a').each do |star|
        stars.push star.content
      end
      stars.delete_at 0
      stars
    end

    def get_movie_director(movie_container)
      director_container = movie_container.css('.lister-item-content p')[2]
      director = director_container.css('a').first
      director.content.strip
    end

    def get_movie_summary(movie_container)
      summary = movie_container.css('.lister-item-content p.text-muted')[1]
      summary.content.strip[0..100]
    end

    def get_movie_rate(movie_container)
      rate = movie_container.css('.lister-item-content .ratings-bar .ratings-imdb-rating strong').first
      rate.content.to_f
    end

    def check_page_count(start_page_count, page_count)
      movie_containers = get_movie_containers start_page_count
      length = movie_containers.length
      total_length = length + start_page_count

      movie_container = movie_containers[249]
      movie_container_index = get_movie_container_index(movie_container)
      row_index_formatted = "#{page_count}."

      if total_length == page_count && movie_container_index == row_index_formatted
        true
      else
        false
      end
    end
  end
end
