class Movie < ApplicationRecord
  has_and_belongs_to_many :genres
  has_and_belongs_to_many :stars, join_table: :stars_movies
  has_many :follows, as: :followable

  validates :name, presence: true
  validates :summary, presence: true
  validates :image, presence: true
  validates :year, presence: true
  validates :rate, presence: true
  validates :time, presence: true
  validates :director, presence: true

  def self.get_by_model_class(object)
    object.movies.order('year DESC').order('rate DESC').limit(5)
  end

  def self.follow(user_id, movie_id)
    follows = Follow.check_is_already_exists_by(user_id, Movie.name, movie_id)
    if follows.first.nil? == false
      follows.each do |follow|
        Suggestion.update_suggestion_for_movie(follow.id, follow.followable_id, user_id)
      end
      follows.delete_all
    else
      created_follow_id = Follow.create_follow_and_get_id(user_id, movie_id, Movie.name)
      Suggestion.update_suggestion_for_movie(created_follow_id, movie_id, user_id, true)
    end
  end

  def self.get_movies(by)
    result = Movie.group(by + ',id').select(by + ',id').order(by + ' DESC').limit(20)
    Movie.where(id: result.pluck(:id)).order("RANDOM()")
  end
end
