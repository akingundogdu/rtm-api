class Genre < ApplicationRecord
  has_and_belongs_to_many :movies
  has_many :follows, as: :followable
  validates :name, presence: true

  def self.follow(user_id, genre_id)
    follows = Follow.check_is_already_exists_by(user_id, Genre.name, genre_id)
    if follows.first.nil? == false
      follows.each do |follow|
        Suggestion.update_suggestion_for_genre(follow.id, follow.followable_id, user_id)
      end
      follows.delete_all
    else
      created_follow_id = Follow.create_follow_and_get_id(user_id, genre_id, Genre.name)
      Suggestion.update_suggestion_for_genre(created_follow_id, genre_id, user_id, true)
    end
  end
end
