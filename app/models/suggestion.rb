class Suggestion < ApplicationRecord
  belongs_to :follow
  belongs_to :user
  belongs_to :movie

  def self.update_suggestion_for_genre(follow_id, genre_id, user_id, can_add = false)
    genre = Genre.find(genre_id)
    movies = Movie.get_by_model_class(genre)
    update_suggestion(movies, follow_id, user_id, can_add)
  end

  def self.update_suggestion_for_star(follow_id, star_id, user_id, can_add = false)
    star = Star.find(star_id)
    movies = Movie.get_by_model_class(star)
    update_suggestion(movies, follow_id, user_id, can_add)
  end

  def self.update_suggestion_for_movie(follow_id, movie_id, user_id, can_add = false)
    movie = Movie.find(movie_id)
    # We can add all genres's and stars's movies into suggestion,
    # But right now we don't need it, I just add one for test
    update_suggestion_for_genre(follow_id, movie.genres.first.id, user_id, can_add)
    update_suggestion_for_star(follow_id, movie.stars.first.id, user_id, can_add)
  end

  def self.update_suggestion(movies, follow_id, user_id, can_add = false)
    movies.each do |movie|
      suggestions = get_suggestion(user_id, follow_id, movie.id)
      suggestions.delete_all
      next unless can_add

      create_suggestion(user_id, follow_id, movie.id)
    end
  end

  def self.create_suggestion(user_id, follow_id, movie_id)
    Suggestion.new(user_id: user_id,
                   follow_id: follow_id,
                   movie_id: movie_id).save
  end

  def self.get_suggestion(user_id, follow_id, movie_id)
    suggestions = Suggestion.where({ user_id: user_id,
                                     #follow_id: follow_id,
                                     movie_id: movie_id })
    suggestions
  end

  def self.get_suggestions(token)
    user = User.find_by_authentication_token token
    Suggestion.where(:user_id => user.id).order("RANDOM()")
   end
end
