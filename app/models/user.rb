# frozen_string_literal: true

class User < ApplicationRecord
  #include GraphQL::Interface

  has_many :follows
  has_many :genres, through: :follows, source: :followable, source_type: 'Genre'
  has_many :stars, through: :follows, source: :followable, source_type: 'Star'
  has_many :movies, through: :follows, source: :followable, source_type: 'Movie'

# Include default devise modules. Others available are:
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :database_authenticatable, :token_authenticatable

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true

  def generate_password_token!
    self.reset_password_token = generate_token
    self.reset_password_sent_at = Time.now.utc
    save!
  end

  def password_token_valid?
    (self.reset_password_sent_at + 4.hours) > Time.now.utc
  end

  def reset_password!(password)
    self.reset_password_token = nil
    self.password = password
    save!
  end

  def self.get_by_token(token)
    User.find_by_authentication_token(token)
  end

  def self.authorize(authorization)
    @verified_message = 'Authorization token not verified'
    raise 'Authorization token not specified' if authorization.nil?
    token = authorization.gsub('Bearer ', '')
    user = get_by_token token
    raise @verified_message if user.nil?
  rescue ActiveRecord::RecordNotFound => e
    raise @verified_message
  end

  def self.generate_security_token(args)
    crypt = ActiveSupport::MessageEncryptor.new(SecureRandom.random_bytes(32))
    token = crypt.encrypt_and_sign(args[:email])
    token
  end

  def self.register_user(args, context)
    token = generate_security_token args
    user = User.new(first_name: args[:first_name],
                    last_name: args[:last_name],
                    email: args[:email],
                    password: args[:password],
                    authentication_token: token).save!
    user = User.find_by_email(args[:email])
    context[:current_user] = user
    Mutations::MutationResult.call(
        obj: {user: user}
    )
  end

  def self.sign_in(args, context)
    user = User.find_by_email(args[:email])
    if user.present?
      if user.valid_password?(args[:password])
        context[:current_user] = user
        Mutations::MutationResult.call(obj: { user: user }, success: true)
      else
        GraphQL::ExecutionError.new('Incorrect email or password')
      end
    else
      GraphQL::ExecutionError.new('User not registered on this application')
    end
  end

  def self.change_password(email, password)
    user = User.find_by_email(email)
    if user.present?
      user.generate_password_token!
      # in the real life, we should send password reset link via email by token in here
      if user.password_token_valid?
        user.reset_password!(password)
        MutationResult.call(success: true)
      else
        GraphQL::ExecutionError.new('User reset token is not valid')
      end
    else
      GraphQL::ExecutionError.new('Please check your email again')
    end
  end

  private

  def generate_token
    SecureRandom.hex(10)
  end
end
