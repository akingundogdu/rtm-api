# frozen_string_literal: true

class Follow < ApplicationRecord
  belongs_to :user
  belongs_to :followable, polymorphic: true
  has_one :genre, class_name: Genre.name, foreign_key: 'id'
  has_one :star, class_name: Star.name, foreign_key: 'id'
  has_one :movie, class_name: Movie.name, foreign_key: 'id'

  def self.create_follow_and_get_id(user_id, followable_id, followable_type)
    follow = Follow.new
    follow.user_id = user_id
    follow.followable_id = followable_id
    follow.followable_type = followable_type
    follow.save
    id = p follow.id
    id
  end

  def self.check_is_already_exists_by(user_id, type, id)
    Follow.where({ user_id: user_id, followable_id: id, followable_type: type })
  end
end
