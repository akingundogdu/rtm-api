class CreateStarsMovies < ActiveRecord::Migration[6.0]
  def change
    create_table :stars_movies, id: false do |t|
      t.integer :star_id
      t.integer :movie_id
    end
  end
end
