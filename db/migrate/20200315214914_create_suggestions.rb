class CreateSuggestions < ActiveRecord::Migration[6.0]
  def change
    create_table :suggestions do |t|
      t.integer :user_id
      t.integer :follow_id
      t.integer :movie_id
      t.timestamps
    end
  end
end
