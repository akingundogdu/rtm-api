# frozen_string_literal: true

FactoryBot.define do
  factory :star do
    sequence(:name) { |n| "John-#{n}" }
  end
end
