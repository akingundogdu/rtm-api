# frozen_string_literal: true

FactoryBot.define do
  factory :movie do
    sequence(:name) { |n| "movie-#{n}-name" }
    image { 'image' }
    summary { 'summary' }
    year { 2019 }
    rate { 7.3 }
    time { '127min' }
    director { 'Director name' }
  end
end
