# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "akin-#{n}@gundogdu.com" }
    first_name { "akin" }
    last_name { "gundogdu" }
    password { "password_sample" }
    authentication_token { 'token' }
    authentication_token_created_at { '12.12.2020' }
  end
end
