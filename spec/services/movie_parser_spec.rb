require 'rails_helper'
require 'open-uri'

RSpec.describe Star, type: :helper do
  before(:each) do
    @movie_parser = ImdbCraw::MoviesParser.new
  end
  def mock_open_uri
    expect(OpenURI).to receive(:open_uri).and_return(File.new("#{Rails.root}/spec/fixtures/top_250_movies.html")).at_most(:twice)
  end
  it 'url should be valid' do
    url = @movie_parser.get_movies_url_by
    expect(url).to eq('https://www.imdb.com/search/title/?title_type=feature,tv_movie,tv_series,tv_episode&groups=top_1000&count=250&start=1')
  end

  it 'should get response from imdb' do
    page_title = @movie_parser.get_page_title
    expect(page_title).to eq('Feature Film/TV Movie/TV Series/TV Episode,
IMDb "Top 1000"
(Sorted by Popularity Ascending) ')
  end

  it 'the page count should be 250 when first page is called' do
    result = @movie_parser.check_first_page_item_count
    expect(result).to eq(true)
  end

  it 'the page count should be 500 when second page is called' do
    result = @movie_parser.check_second_page_item_count
    expect(result).to eq(true)
  end

  it 'the page count should be 750 when third page is called' do
    result = @movie_parser.check_third_page_item_count
    expect(result).to eq(true)
  end

  it 'the page count should be 1000 when fourth page is called' do
    result = @movie_parser.check_fourth_page_item_count
    expect(result).to eq(true)
  end

  context 'with mocking open-uri' do
    before(:each) do
      mock_open_uri
      @movie_parser = ImdbCraw::MoviesParser.new
    end
    it 'should see image url of first movie' do
      image = @movie_parser.get_movie_image @movie_parser.get_first_movie_container
      expect(image).to eq('https://m.media-amazon.com/images/M/MV5BMGUwZjliMTAtNzAxZi00MWNiLWE2NzgtZGUxMGQxZjhhNDRiXkEyXkFqcGdeQXVyNjU1NzU3MzE@._V1_UY800_CR3,0,500,800_AL_.jpg')
    end

    it 'should see title of first movie' do
      title = @movie_parser.get_movie_title @movie_parser.get_first_movie_container
      expect(title).to eq('Knives Out')
    end

    it 'should see year of first movie' do
      year = @movie_parser.get_movie_year @movie_parser.get_first_movie_container
      expect(year).to eq(2019)
    end

    it 'should see time of first movie' do
      time = @movie_parser.get_movie_time @movie_parser.get_first_movie_container
      expect(time).to eq('131 min')
    end

    it 'should see rate of first movie' do
      rate = @movie_parser.get_movie_rate @movie_parser.get_first_movie_container
      expect(rate).to eq(8.0)
    end

    it 'should see genres of first movie' do
      genres = @movie_parser.get_movie_genres @movie_parser.get_first_movie_container
      expect(genres).to eq(['Comedy', ' Crime', ' Drama'])
    end

    it 'should see summary of first movie' do
      summary = @movie_parser.get_movie_summary @movie_parser.get_first_movie_container
      expect(summary).to eq('A detective investigates the death of a patriarch of an eccentric, combative family.')
    end

    it 'should see director of first movie' do
      director = @movie_parser.get_movie_director @movie_parser.get_first_movie_container
      expect(director).to eq('Rian Johnson')
    end

    it 'should see stars of first movie' do
      stars = @movie_parser.get_movie_stars @movie_parser.get_first_movie_container
      expect(stars).to eq(["Daniel Craig", "Chris Evans", "Ana de Armas", "Jamie Lee Curtis"])
    end
  end

  # context 'with integration test' do
  #   it 'should parse from imdb and add all data into database' do
  #     @movie_parser = ImdbCraw::MoviesParser.new
  #     @movie_parser.parse_all_movies_by 0
  #     expect(Movie.count).to eq(250)
  #     @movie_parser.parse_all_movies_by 250
  #     expect(Movie.count).to eq(500)
  #     @movie_parser.parse_all_movies_by 500
  #     expect(Movie.count).to eq(750)
  #     @movie_parser.parse_all_movies_by 750
  #     expect(Movie.count).to eq(1000)
  #   end
  # end
end