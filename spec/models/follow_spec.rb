require 'rails_helper'

RSpec.describe Follow, type: :model do
  it { should belong_to(:user) }
  it { should belong_to(:followable) }

  context 'for following' do
    before(:each) do
      User.new(attributes_for(:user)).save!
      FactoryBot.create_list(:genre, 2)
      FactoryBot.create_list(:star, 2)
      FactoryBot.create_list(:movie, 2)
    end
    it 'ensures created user' do
      expect(User.all.size).to eq(1)
    end
    it 'ensures created genres' do
      expect(Genre.all.size).to eq(2)
    end
    it 'ensures created stars' do
      expect(Star.all.size).to eq(2)
    end
    it 'ensures created movies' do
      expect(Movie.all.size).to eq(2)
    end
    it 'can follow first genre' do
      user = User.first
      genre = Genre.first
      expect(Follow.all.size).to eq(0)
      Follow.new(user_id: user.id,
                          followable_id: genre.id,
                          followable_type: Genre.name).save!
      follow = Follow.first
      expect(follow.genre).to eq(genre)
      expect(follow.user_id).to eq(user.id)
      expect(follow.followable_id).to eq(genre.id)
      expect(follow.followable_type).to eq(Genre.name)
      expect(Follow.all.size).to eq(1)
    end
    it 'can follow first star' do
      user = User.first
      star = Star.first
      expect(Follow.all.size).to eq(0)
      Follow.new(user_id: user.id,
                 followable_id: star.id,
                 followable_type: Star.name).save!
      follow = Follow.first
      expect(follow.star).to eq(star)
      expect(follow.user_id).to eq(user.id)
      expect(follow.followable_id).to eq(star.id)
      expect(follow.followable_type).to eq(Star.name)
      expect(Follow.all.size).to eq(1)
    end
    it 'can follow first movie' do
      user = User.first
      movie = Movie.first
      expect(Follow.all.size).to eq(0)
      Follow.new(user_id: user.id,
                 followable_id: movie.id,
                 followable_type: Movie.name).save!
      follow = Follow.first
      expect(follow.movie).to eq(movie)
      expect(follow.user_id).to eq(user.id)
      expect(follow.followable_id).to eq(movie.id)
      expect(follow.followable_type).to eq(Movie.name)
      expect(follow.user).to eq(user)
      expect(Follow.all.size).to eq(1)
    end
  end
end
