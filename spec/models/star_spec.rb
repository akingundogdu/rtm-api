require 'rails_helper'

RSpec.describe Star, type: :model do
  it { should have_and_belong_to_many(:movies) }
  it { should have_many(:follows) }

  context 'validation tests' do
    it 'ensures name presence' do
      star = Star.new(nil).save
      expect(star).to eq(false)
    end

    it 'should save successfully' do
      star = Star.new(name: ' Marlon Brando').save!
      expect(star).to eq(true)
    end

    context 'scope tests' do
      before(:each) do
        Star.new(name: 'Marlon Brando').save!
        Star.new(name: 'Al Pacino').save!
        Star.new(name: 'Diane Keaton').save!
      end

      it 'should return 3 stars' do
        expect(Star.all.size).to eq(3)
      end

      it 'should return Marlon Brando when called the first item' do
        expect(Star.first.name).to eq('Marlon Brando')
      end

      it 'should return Diane Keaton when called the last item' do
        expect(Star.last.name).to eq('Diane Keaton')
      end

      it 'can see movie data of star' do
        Movie.delete_all
        Star.delete_all
        Movie.new(attributes_for(:movie)).save!
        Star.new(attributes_for(:star)).save!
        movie = Movie.first
        star = Star.first
        expect(Movie.all.size).to eq(1)
        expect(Star.all.size).to eq(1)
        expect(star.movies.all.size).to eq(0)
        expect(movie.stars.all.size).to eq(0)
        star.movies << movie
        expect(star.movies.all.size).to eq(1)
        expect(movie.stars.all.size).to eq(1)
      end
    end
  end
end
