require 'rails_helper'

RSpec.describe Movie, type: :model do
  it { should have_many(:follows) }
  it { should have_and_belong_to_many(:genres) }
  it { should have_and_belong_to_many(:stars) }

  context 'validation tests' do
    it 'ensures given error without any attributes' do
      movie = Movie.new(nil).save
      expect(movie).to eq(false)
    end

    it 'ensures name presence' do
      movie = Movie.new(image: 'fake_image',
                        year: 2019,
                        summary: 'summary',
                        rate: 6.7,
                        time: '127min',
                        director: 'Francis Ford').save
      expect(movie).to eq(false)
    end

    it 'ensures image presence' do
      movie = Movie.new(name: 'Jumanji',
                        summary: 'summary',
                        year: 2019,
                        rate: 6.7,
                        time: '127min',
                        director: 'Francis Ford').save
      expect(movie).to eq(false)
    end

    it 'ensures year presence' do
      movie = Movie.new(name: 'Jumanji',
                        summary: 'summary',
                        image: 'fake_image',
                        rate: 6.7,
                        time: '127min',
                        director: 'Francis Ford').save
      expect(movie).to eq(false)
    end

    it 'ensures rate presence' do
      movie = Movie.new(name: 'Jumanji',
                        summary: 'summary',
                        image: 'fake_image',
                        year: 2019,
                        time: '127min',
                        director: 'Francis Ford').save
      expect(movie).to eq(false)
    end

    it 'ensures time presence' do
      movie = Movie.new(name: 'Jumanji',
                        summary: 'summary',
                        image: 'fake_image',
                        year: 2019,
                        rate: 6.7,
                        director: 'Francis Ford').save
      expect(movie).to eq(false)
    end

    it 'ensures director presence' do
      movie = Movie.new(name: 'Jumanji',
                        summary: 'summary',
                        image: 'fake_image',
                        year: 2019,
                        rate: 6.7,
                        time: '127min').save
      expect(movie).to eq(false)
    end

    it 'ensures summary presence' do
      movie = Movie.new(name: 'Jumanji',
                        image: 'fake_image',
                        year: 2019,
                        rate: 6.7,
                        time: '127min').save
      expect(movie).to eq(false)
    end

    it 'should save successfully' do
      movie = Movie.new(name: 'Jumanji',
                        image: 'fake_image',
                        summary: 'summary',
                        year: 2019,
                        rate: 6.7,
                        time: '127min',
                        director: 'Francis Ford').save!
      expect(movie).to eq(true)
    end

    context 'scope tests' do
      before(:each) do
        Movie.new(name: 'Jumanji1',
                  image: 'fake_image',
                  summary: 'summary',
                  year: 2019,
                  rate: 6.7,
                  time: '127min',
                  director: 'Francis Ford').save!

        Movie.new(name: 'Jumanji2',
                  image: 'fake_image',
                  summary: 'summary',
                  year: 2019,
                  rate: 6.7,
                  time: '127min',
                  director: 'Francis Ford').save!

        Movie.new(name: 'Jumanji3',
                  image: 'fake_image3',
                  summary: 'summary3',
                  year: 2020,
                  rate: 7.0,
                  time: '127min3',
                  director: 'Francis Ford3').save!
      end

      it 'should return 3 movies' do
        expect(Movie.all.size).to eq(3)
      end

      it 'should return Jumanji1 when called the first item' do
        expect(Movie.first.name).to eq('Jumanji1')
      end

      it 'should return Jumanji3 when called the last item' do
        movie = Movie.last
        expect(movie.name).to eq('Jumanji3')
        expect(movie.image).to eq('fake_image3')
        expect(movie.summary).to eq('summary3')
        expect(movie.year).to eq(2020)
        expect(movie.rate).to eq(7.0)
        expect(movie.time).to eq('127min3')
        expect(movie.director).to eq('Francis Ford3')
      end

      context 'assigns validation' do
        before(:each) do
          User.new(attributes_for(:user)).save!
          Movie.new(attributes_for(:movie)).save!
        end
        it 'can see follow data of movie ' do
          user = User.first
          movie = Movie.first
          expect(Follow.all.size).to eq(0)
          Follow.new(user_id: user.id,
                     followable_id: movie.id,
                     followable_type: Movie.name).save!
          follow = Follow.first
          expect(follow.movie).to eq(movie)
          expect(follow.user_id).to eq(user.id)
          expect(follow.followable_id).to eq(movie.id)
          expect(follow.followable_type).to eq(Movie.name)
          expect(follow.user).to eq(user)
          expect(movie.follows).to contain_exactly(follow)
          expect(Follow.all.size).to eq(1)
        end

        it 'can see genre data of movie ' do
          Genre.new(attributes_for(:genre)).save!
          genre = Genre.first
          movie = Movie.first
          expect(movie.genres.all.size).to eq(0)
          movie.genres << genre
          expect(movie.genres.all.size).to eq(1)
          expect(movie.genres.first).to eq(genre)
        end

        it 'can see star data of movie ' do
          Star.new(attributes_for(:genre)).save!
          star = Star.first
          movie = Movie.first
          expect(movie.stars.all.size).to eq(0)
          movie.stars << star
          expect(movie.stars.all.size).to eq(1)
          expect(movie.stars.first).to eq(star)
        end
      end
    end
  end
end
