require 'rails_helper'

RSpec.describe Genre, type: :model do
  it { should have_many(:follows) }
  it { should have_and_belong_to_many(:movies) }

  context 'validation tests' do
    it 'ensures name presence' do
      genre = Genre.new(nil).save
      expect(genre).to eq(false)
    end

    it 'should save successfully' do
      genre = Genre.new(name: 'Comedy').save
      expect(genre).to eq(true)
    end

    context 'scope tests' do
      before(:each) do
        Genre.new(name: 'Comedy').save!
        Genre.new(name: 'Action').save!
        Genre.new(name: 'Drama').save!
      end

      it 'should return 3 genres' do
        expect(Genre.all.size).to eq(3)
      end

      it 'should return Comedy when called the first item' do
        expect(Genre.first.name).to eq('Comedy')
      end

      it 'should return Drama when called the last item' do
        expect(Genre.last.name).to eq('Drama')
      end

      it 'can see movie data of genre' do
        Movie.delete_all
        Genre.delete_all
        Movie.new(attributes_for(:movie)).save!
        Genre.new(attributes_for(:genre)).save!
        movie = Movie.first
        genre = Genre.first
        expect(Movie.all.size).to eq(1)
        expect(Genre.all.size).to eq(1)
        expect(genre.movies.all.size).to eq(0)
        expect(movie.genres.all.size).to eq(0)
        genre.movies << movie
        expect(genre.movies.all.size).to eq(1)
        expect(movie.genres.all.size).to eq(1)
      end
    end
  end
end
