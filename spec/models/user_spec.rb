require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_many(:follows) }
  it { should have_many(:stars) }
  it { should have_many(:genres) }
  it { should have_many(:movies) }

  context 'validation tests' do
    it 'ensures given error without any attributes' do
      user = User.new(nil).save
      expect(user).to eq(false)
    end

    it 'ensures first name presence' do
      user = User.new(last_name: 'gundogdu', email: 'akin@gundogdu.com').save
      expect(user).to eq(false)
    end

    it 'ensures last name presence' do
      user = User.new(first_name: 'akin', email: 'akin@gundogdu.com').save
      expect(user).to eq(false)
    end

    it 'ensures email presence' do
      user = User.new(first_name: 'akin', last_name: 'gundogdu').save
      expect(user).to eq(false)
    end

    it 'should save successfully' do
      user = User.new(first_name: 'akin',
                      last_name: 'gundogdu',
                      email: 'akin@gundogdu.com',
                      password: 'sample_password',
                      authentication_token: 'fake_token',
                      authentication_token_created_at: '12.12.2020').save!

      expect(user).to eq(true)
    end

    context 'scope tests' do
      before(:each) do
        User.new(first_name: 'akin1',
                 last_name: 'gundogdu1',
                 email: 'akin1@gundogdu.com',
                 password: 'password1',
                 authentication_token: 'fake_token1',
                 authentication_token_created_at: '12.12.2020').save!

        User.new(first_name: 'akin2',
                 last_name: 'gundogdu2',
                 email: 'akin2@gundogdu.com',
                 password: 'password2',
                 authentication_token: 'fake_token2',
                 authentication_token_created_at: '12.12.2020').save!

        User.new(first_name: 'akin3',
                 last_name: 'gundogdu3',
                 email: 'akin3@gundogdu.com',
                 password: 'password3',
                 authentication_token: 'fake_token3',
                 authentication_token_created_at: '12.12.2020').save!
      end

      it 'should return 3 users' do
        expect(User.all.size).to eq(3)
      end

      it 'should return akin1 when called the first item' do
        user = User.first!
        expect(user.first_name).to eq('akin1')
        expect(user.last_name).to eq('gundogdu1')
        expect(user.email).to eq('akin1@gundogdu.com')
      end

      it 'should return akin3 when called the last item' do
        user = User.last!
        expect(user.first_name).to eq('akin3')
        expect(user.last_name).to eq('gundogdu3')
        expect(user.email).to eq('akin3@gundogdu.com')
      end
    end
  end
end
