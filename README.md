# Welcome to RTM Backend app documentation



Remote team movie is a technical test project for senior software engineer role of remote team company. 

This project is divided into two as frontend and backend and consists of separate projects.

  - You can access app online from [client-online]
  - You can access source code of client app from [client-source-code]
  - You can access backend api online from [backend-online] if you want
  - You can access source code of backend api from [backend-source-code]


# Prerequisites

>Before we begin, make sure you have [Ruby] version >=2.6.3 and rails version 6.0.2.


```sh
$ ruby -v # ruby 2.6.3
$ rails -v # Rails 6.0.2
```


### RubyGems
As you know Ruby on rails utilizes [RubyGem] to manage its dependencies. So, before starting setup RTM app, make sure you have [RubyGem] installed on your machine.

You can check if you have 
```sh
$ which gem
```

To update to its latest version with:

```sh
$ gem update --system
```

# Project Setup

First step, we need to clone backend api repo to your local machine.
```sh
$ git clone https://gitlab.com/akingundogdu/rtm-api.git
```
After that, go to inside directory your cloned path and running this command.
```sh
$ bundle install
```
### Dependencies
Let's take a moment to review the gems that we used.

  - [rspec-rails](https://github.com/rspec/rspec-rails) - Testing framework.
  - [factory_bot_rails](https://github.com/thoughtbot/factory_bot_rails) - A fixtures replacement with a more straightforward syntax. You'll see.
  - [shoulda_matchers](https://github.com/thoughtbot/shoulda-matchers) - Provides RSpec with additional matchers.
  - [database_cleaner](https://github.com/DatabaseCleaner/database_cleaner) - You guessed it! It literally cleans our test database to ensure a clean state in each test suite.
  - [faker](https://github.com/faker-ruby/faker) - A library for generating fake data. We'll use this to generate test data.
  - [devise](https://rubygems.org/gems/devise/versions/4.2.0) - Flexible authentication solution for Rails with Warden
  - [graphql](https://graphql-ruby.org/) - GraphQL is a query language for APIs and a runtime for fulfilling those queries with your existing data.
  - [Nokogiri](https://nokogiri.org) - Nokogiri is an HTML, XML, SAX, and Reader parser.
  - [figaro](https://rubygems.org/gems/figaro/versions/1.1.1) - Simple, Heroku-friendly Rails app configuration using ENV and a single YAML file
  - [rack-cors](https://rubygems.org/gems/figaro/versions/1.1.1) - Rack Middleware for handling Cross-Origin Resource Sharing (CORS), which makes cross-origin AJAX possible.
  - [pg](https://rubygems.org/gems/pg/versions/0.18.4) - Pg is the Ruby interface to the {PostgreSQL RDBMS}`
  - [graphiql-rails](https://github.com/rmosolgo/graphiql-rails) - Mount the GraphiQL query editor in a Rails app
  - [simplecov](https://github.com/colszowka/simplecov) - Code coverage for Ruby 1.9+ with a powerful configuration library and automatic merging of coverage across test suites

### Unit Test

In your root directory, run this command and see all tests are success, like below image

 ```sh
$ rspec
```

![Figure 1-1](storage/rtm-unit-test.png "Figure 1-1")

 
 ### Run Project
For starting serve backend api, just you need to run this command
 ```sh
$ rails server -p 5000
```

All done!

If you want to see all application logic and user interfaces, you can read [client-side](https://gitlab.com/akingundogdu/rtm-app.git) app document




# Tech

- Ruby on Rails
- GraphQL
- GraphiQL
- RubyMine
- Ohmzsh
- Postman
- MacOS





### About me
Thanks for reading.

- I'm Akin Gundogdu
- Email : akin-gundogdu@hotmail.com
- Linkedin : https://www.linkedin.co.uk/in/akingundogdu







   [client-online]: <https://rtm-app-client.herokuapp.com>
   [backend-online]: <http://rtm-app-api.herokuapp.com/>
   [backend-source-code]: <https://gitlab.com/akingundogdu/rtm-api.git>
   [client-source-code]: <https://gitlab.com/akingundogdu/rtm-app.git>
   [RubyGem]: <http://rubygems.org/>
   [Ruby]: <https://rubyonrails.org/>
